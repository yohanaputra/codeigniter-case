<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->myloader = new MY_Loader;
        $this->data['controller'] = $this->router->fetch_class();
        $this->data['view'] = $this->myloader->_view();
        $this->data['template'] = $this->myloader->_layouts();
        $this->data['method'] = $this->router->fetch_method();
    }

    

}

?>
