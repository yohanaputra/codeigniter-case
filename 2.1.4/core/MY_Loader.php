<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH . "third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {

    public function __construct() {
        parent::__construct();
    }

    public function _assets() {
        echo MYROOT . 'assets/';
    }

    public function _layouts() {
        return 'layouts/';
    }

    public function _view() {
        return $this->router->fetch_class() . '/' . $this->router->fetch_method();
    }

    public function _web_name() {
        return $this->config->item('gl-name');
    }

    public function _year_development() {
        return $this->config->item('gl-year');
    }

    public function _company_info($info = "name") {
        switch ($info) {
            case "name":
                return $this->config->item('company-name');
                break;
            case "uri":
                return $this->config->item('company-web');
                break;
            case "author":
                return $this->config->item('company-author');
                break;
        }
    }

    public function _load_plugin($plugin = null) {
        $this->load->view('plugins/' . $plugin . '/' . $plugin);
    }

    public function _get_thirdy_party($plugin = null) {
        return 'thirdy-party/' . $plugin . '/' . $plugin;
    }

    public function _plugin_assets($plugin = "") {
        return MYROOT . 'assets/plugins/' . $plugin . '/';
    }

    public function _upload_path() {
        return MYROOT . 'media/';
    }

}