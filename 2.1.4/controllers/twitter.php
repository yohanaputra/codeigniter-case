<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Twitter extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->data['title'] = 'Twitter';
        $this->data['username'] = 'username';
        $this->data['screenname'] = 'screenname';
    }

    public function index() {
        $this->data['view'] = $this->data['view'];
        $this->parser->parse($this->data['template'] . 'index', $this->data);
    }
    function getConnectionWithAccessToken($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
        $connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
        return $connection;
    }

    public function twit() {
        $twitteruser = $this->data['username'];
        $notweets = 30;
        $consumerkey = "xxx";
        $consumersecret = "xxx";
        $accesstoken = "xxx";
        $accesstokensecret = "xxx";

        $connection = $this->getConnectionWithAccessToken($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);

        $tweets = $connection->get( "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" . $twitteruser . "&count=" . $notweets);

        echo json_encode($tweets);
    }

}
