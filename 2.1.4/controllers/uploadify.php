<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Uploadify extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = 'Uploadify';
    }

    public function index() {
        $dir = array_diff(scandir("media/"), array(".", ".."));

        $content = "";
        $content .= "<ul>";
        foreach ($dir as $f) {
            $content .= "<li><img width='128' src='" . MYROOT . "media/" . $f . "' /></li>";
        }
        $content .= "</ul>";
        $this->data['content']= $content;
        $this->data['view'] = $this->data['view'];

        $this->parser->parse($this->data['template'] . 'index', $this->data);
    }

    public function uploader($id = null) {
        $data = $this->input->get("id_product");
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
        $targetFile = str_replace('//', '/', $targetPath) . $_FILES['Filedata']['name'];

        move_uploaded_file($tempFile, $targetFile);
        echo str_replace($_SERVER['DOCUMENT_ROOT'], '', $targetFile);
    }

    public function showdir() {
        $dir = array_diff(scandir("media/"), array(".", ".."));

        $content = "";
        $content .= "<ul>";
        foreach ($dir as $f) {
            $content .= "<li><img width='128' src='" . MYROOT . "media/" . $f . "' /></li>";
        }
        $content .= "</ul>";
        echo $content;
    }

}
