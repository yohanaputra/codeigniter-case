<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = 'HomePages';
    }

    public function index() {
        $this->data['view'] = $this->data['view'];
        $this->parser->parse($this->data['template'].'index', $this->data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */