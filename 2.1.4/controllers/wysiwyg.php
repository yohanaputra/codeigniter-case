<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wysiwyg extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = 'Wysiwyg TinyMCE';
    }

    public function index() {
        $this->data['view'] = $this->data['view'];
        $this->parser->parse($this->data['template'] . 'index', $this->data);
    }

    public function add() {
        $this->data['content'] = "";
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $data = $this->input->post('input');
            $this->data['content'] = $data['ajaxfilemanager'];
        }
        $this->data['mainview'] = $this->data['view'];
        $this->parser->parse($this->data['template'] . 'index', $this->data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */