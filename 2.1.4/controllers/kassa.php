<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kassa extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = 'Kassa';
    }

    public function index() {
        $this->load->library('cart');
        $this->data['view'] = $this->data['view'];
        $this->parser->parse($this->data['template'] . 'index', $this->data);
    }

    public function additem() {
        $this->load->library('cart');
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $data = $this->input->post();
            $cart = array(
                'id' => $data["itemid"],
                'qty' => $data["itemqty"],
                'price' => $data["itemprice"],
                'name' => $data["itemname"]
            );
            $this->cart->insert($cart);
            $this->data['cartlist'] = $this->cart->contents();
            $this->load->view($this->data['view'], $this->data);
        } else {
            $this->cart->destroy();
            redirect("kassa");
        }
    }

}
