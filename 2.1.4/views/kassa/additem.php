<?php $i = 1; ?>

<?php foreach ($cartlist as $items) : ?>
    <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
    <tr>
        <td><?php echo form_input(array('name' => $i . '[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?></td>
        <td>
            <?php echo $items['name']; ?>

            <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                <p>
                    <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                        <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
                    <?php endforeach; ?>
                </p>
            <?php endif; ?>
        </td>
        <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
        <td style="text-align:right"><?php echo $this->cart->format_number($items['subtotal']); ?></td>
        <td style="text-align:right">
            <a class="linkedit">Edit</a>
            <a class="linkdelete">Delete</a>
        </td>
    </tr>
    <?php $i++; ?>
<?php endforeach; ?>
