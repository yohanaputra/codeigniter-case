<?php $this->_load_plugin('uploadify'); ?>
<form>
    <div id="queue"></div>
    <input id="file_upload" name="file_upload" type="file" multiple="true">
</form>
<div id="imglists">
    <?php echo $content; ?>
</div>
<script type="text/javascript">
<?php $timestamp = time(); ?>
    $(function() {
        $('#file_upload').uploadify({
            'formData'     : {
                'timestamp' : '<?php echo $timestamp; ?>',
                'token'     : '<?php echo md5('unique_salt' . $timestamp); ?>',
                'folder' : '<?php echo $this->_upload_path() ?>'
            },
            'swf'      : '<?php echo $this->_plugin_assets('uploadify') ?>uploadify.swf',
            'uploader' : '<?php echo base_url('uploadify/uploader') ?>',
            'onUploadSuccess' : function(file, data, response) {
                $.post("<?php echo base_url('uploadify/showdir') ?>", {
                    
                }, function(e){
                    $("#imglists").html("");
                    $("#imglists").html(e);
                });
            }
        });
    });
</script>
