<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Codeigniter Case</title>
        <link href="<?php $this->_assets();?>layouts/css/style.css" type="text/css" media="all" rel="stylesheet"/>

    </head>
    <body>
        <div id ="menu">
            <ul>
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url('wysiwyg')?>">wyswig tiny-mce</a></li>
                <li><a href="<?php echo base_url('uploadify')?>">Uploadify</a></li>
                <li><a href="<?php echo base_url('twitter')?>">Twitter</a></li>
            </ul>
        </div>
        <div id="container">
            <h1>{title}</h1>

            <div id="body">
                <?php $this->load->view($view); ?>

                <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
            </div>
        </div>

    </body>
</html>