<script type="text/javascript" src="<?php echo $this->_plugin_assets('jquery') ?>jquery-1.10.2.min.js"></script>

<script type="text/javascript" src="<?php echo $this->_plugin_assets('tiny-mce') ?>jscripts/tiny_mce/jquery.tinymce.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $this->_plugin_assets('ajaxfilemanager') ?>jscripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript">
  $.noConflict();
  // Code that uses other library's $ can follow here.
</script>

<script language="javascript" type="text/javascript">
  var valid_elms    = "img[style|href|src|name|title|onclick|align|alt|title|width|height|vspace|hspace]";

  tinyMCE.init({
    mode : "exact",
    elements : "ajaxfilemanager",
    theme : "advanced",
    //plugins : "style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",
    plugins : "advimage,advlink,media,contextmenu",
    theme_advanced_buttons1_add_before : "newdocument,separator",
    theme_advanced_buttons1_add : "fontselect,fontsizeselect",
    theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
    theme_advanced_buttons2_add_before: "cut,copy,separator,",
    theme_advanced_buttons3_add_before : "",
    theme_advanced_buttons3_add : "media",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    file_browser_callback : "ajaxfilemanager",
    paste_use_dialog : false,
    theme_advanced_resizing : true,
    theme_advanced_resize_horizontal : true,
    apply_source_formatting : false,
    force_br_newlines : false,
    force_p_newlines : true,	
    relative_urls : false,
    extended_valid_elements: valid_elms
  });

  function ajaxfilemanager(field_name, url, type, win) {
    var ajaxfilemanagerurl = "<?php echo $this->_plugin_assets('ajaxfilemanager') ?>jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
    var view = 'detail';
    switch (type) {
      case "image":
        view = 'thumbnail';
        break;
      case "media":
        break;
      case "flash": 
        break;
      case "file":
        break;
      default:
        return false;
    }
    tinyMCE.activeEditor.windowManager.open({
      url: "<?php echo $this->_plugin_assets('ajaxfilemanager') ?>jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
      width: 782,
      height: 440,
      inline : "yes",
      close_previous : "no"
    },{
      window : win,
      input : field_name
    });
  }
</script>
